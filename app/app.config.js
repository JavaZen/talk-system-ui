'use strict';

talkApp.config(['$locationProvider' ,'$routeProvider', '$httpProvider',
    function config($locationProvider, $routeProvider, $httpProvider) {
        $locationProvider.hashPrefix('!');

        $httpProvider.interceptors.push('authRequestInterceptor');

        $routeProvider.
        when('/signin', {
            template: '<sign-in></sign-in>'
        }).
        when('/signup', {
            template: '<sign-up></sign-up>'
        }).
        when('/profile/:userId', {
            template: '<profile></profile>'
        }).
        otherwise('/talkkbs');
    }
]).factory('authRequestInterceptor', [ '$q', '$location',
    function ($q, $location) {
        return {
            request: function (config) {
                if (sessionStorage.getItem('Authorization')) {
                    config.headers.Authorization = sessionStorage.getItem('Authorization');
                }
                return config || $q.when(config);
            },
            responseError: function(rejection) {
                if (rejection.status == 401) {
                    //localStorage.removeItem('user_data');
                    sessionStorage.clear();
                    $location.path('/signin');
                }
                return $q.reject(rejection);
            }
        }
    }
]);
