'use strict';

// Define the `talkApp` module
var talkApp = angular.module('talkApp', [
    'ngAnimate',
    'ngRoute',
    'ui.select',
    'oi.select',
    'signIn',
    'signUp',
    'group',
    'keyword',
    'response',
    'conversation',
    'talkkb',
    'zen.message',
    'longPolling',
    'user',
    'zen.navigation'
]);
talkApp.value('backendUrl', 'http://localhost:8080/api/');