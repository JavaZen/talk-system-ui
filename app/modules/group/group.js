'use strict';

var group = angular.module('group', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination']);

group.component('group', {
    templateUrl: "modules/group/group.template.html",
    controller: function ($scope, $routeParams, groupService) {
        $scope.group;
        $scope.id = $routeParams.groupId;

        groupService.getById($scope.id).then(function(data) {
            $scope.group = data;
        });
    }
});

group.factory('groupService', function ($http, $log, backendUrl) {
    return {
        getById: function (id) {
            return $http.get(backendUrl + '/group/' + id + '/get')
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        groupList: function () {
            return $http.get(backendUrl + '/group/get/all')
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        addGroup: function (group) {
            return $http.post(backendUrl + '/group/add', group)
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        }
    };
});

group.config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/groups', {
            template: '<group-list></group-list>'
        }).
        when('/group/add', {
            template: '<group-add></group-add>'
        }).
        when('/group/:groupId', {
            template: '<group></group>'
        });
    }
]);
