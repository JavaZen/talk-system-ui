'use strict';

group.component('groupAdd', {
    templateUrl: "modules/group/add/group-add.template.html",
    controller: function ($scope, $log, $location, groupService) {

        $scope.newGroup = { name: "", description: ""/*, revenues_when: new Date().getYear() + 1900*/ };

        $scope.addGroup = function () {
            groupService.addGroup($scope.newGroup).then(function(data) {
                $scope.id = data;
            });
            $location.url('!/groups');
        };
    }
});