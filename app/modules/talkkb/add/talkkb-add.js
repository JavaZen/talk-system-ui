'use strict';

talkkb.component('talkkbAdd', {
    templateUrl: "modules/talkkb/add/talkkb-add.template.html",
    controller: function ($scope, $log, $location, talkkbService) {

        $scope.newTalkkb = { name: "", description: "" };

        $scope.addTalkkb = function () {

            if ($scope.addtkb.$invalid) {
                return;
            }
            talkkbService.add($scope.newTalkkb).then(function(data) {
                $log.error(data);
                $location.path('/talkkb/' + data);
            });
        };
    }
});