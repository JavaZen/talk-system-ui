'use strict';

response.component('responseList', {
    templateUrl: "modules/talkkb/response/list/response-list.template.html",
    bindings: {
        kbaseId: '@'
    },
    controller: function ($scope, $routeParams, responseService, keywordService, $log, messageService, userService, talkkbService) {
        var $ctrl = this;
        $ctrl.kb = {};

        $ctrl.templateResponse = {
            "id": undefined,
            "text": undefined,
            "type": undefined,
            "flags": [],
            "knowledge_base_id": undefined,
            "related_responses": [],
            "keywords": []
        };
        $ctrl.editCtrlStates = [{value:'none', show:false}, {value:'edit', show:true},
            {value:'add_child', show:true}, {value:'add_root', show:true}];

        $scope.iframeHeight = window.innerHeight;

        $scope.kbaseId = this.kbaseId;
        $scope.responseTypes = [];
        $scope.rootHolder = [];
        $scope.rootKeywords = [];
        $scope.flags = [];
        $scope.typesOfRelations = [];

        $scope.holder = [];
        $scope.response = undefined;
        $scope.editResponse = undefined;
        $scope.editView = $ctrl.editCtrlStates[0];

        $scope.currentUser = userService.getCurrentUser();
        $scope.isAdmin = userService.hasRole("ROLE_ADMIN");

        talkkbService.getById($scope.kbaseId).then(function (data) {
            $ctrl.kb = data;
            $scope.kb = data;
        });
        
        responseService.getResponseTypes().then(function (types) {
            $scope.responseTypes = types;
        });

        responseService.getFlags().then(function (data) {
            $scope.flags = data;
        });

        responseService.typesOfRelations().then(function (data) {
            $scope.typesOfRelations = data;
        });

        responseService.getTreeByKbId($scope.kbaseId).then(function(data) {
            $scope.rootHolder = data;
        });

        keywordService.getListByKbId($scope.kbaseId).then(function(data) {
            $scope.rootKeywords = data;
        });

        $ctrl.setSelectedResponse = function (container, holder) {
            $scope.reset();
            $scope.response = container.response;

            $scope.container = container;
            $scope.holder = holder;
            $ctrl.loadKeywords($scope.response.id);
        };

        $scope.reset = function () {
            $scope.editView = $ctrl.editCtrlStates[0];
            $scope.response = undefined;
            $scope.editResponse = undefined
        };

        $scope.edit = function (isEdit) {
            $scope.editView = $ctrl.editCtrlStates[isEdit?1:0];
            $scope.editResponse = isEdit ? $scope.response : undefined;

            if (isEdit) { //todo
                keywordService.getListByKbId($scope.kbaseId).then(function(data) {
                    $scope.rootKeywords = data;
                });
            }
        };

        $scope.addChild = function (isAdd) {
            $scope.editView = $ctrl.editCtrlStates[isAdd?2:0];
            $scope.editResponse = angular.copy($ctrl.templateResponse);

            if (isAdd) { //todo
                keywordService.getListByKbId($scope.kbaseId).then(function(data) {
                    $scope.rootKeywords = data;
                });
            }
        };

        $scope.addRoot = function (isAdd) {
            if (isAdd) {
                $scope.editResponse = angular.copy($ctrl.templateResponse);
                $scope.response = angular.copy($ctrl.templateResponse);
                $scope.editView = $ctrl.editCtrlStates[3];

                //todo
                keywordService.getListByKbId($scope.kbaseId).then(function(data) {
                    $scope.rootKeywords = data;
                });
            } else {
                $scope.reset();
            }
        };

        $ctrl.loadKeywords = function (responseId) {
            keywordService.getListByResponseId(responseId).then(function (data) {
                $log.info(data);
                $scope.response.keywords = data;
            });
        };
        
        $scope.addResponse = function () {
            if ($scope.editResponseForm.$invalid) {
                return;
            }

            var response = {
                text: $scope.editResponse.text,
                knowledge_base_id: $scope.kbaseId,
                type: $scope.editResponse.type,
                flags: $scope.editResponse.flags,
                keywords: $scope.editResponse.keywords,
                context_behavior: $scope.editResponse.context_behavior
            };
            var isAddRoot = $scope.editView.value == 'add_root';
            var container = [];

            if(isAddRoot) {
                response.is_root = true;
                container = $scope.rootHolder;
            } else {
                response.is_root = false;
                response.parent_id = $scope.response.id;
                container = $scope.response.related_responses;
            }

            responseService.addResponse(response).then(function(data) {
                $log.info("response  success added", data);
                container.push({ response: data });
            });

            $scope.reset();
        };

        $scope.updateResponse = function (response) {
            if ($scope.editResponseForm.$invalid) {
                return;
            }
            responseService.update(response).then(function (data) {
                $log.info("response with id " + response.id + " success updated")
            });
            $scope.reset();
        };

        $scope.deleteResponse = function (container) {
            $('#deleteResponsePopup').modal('hide');
            responseService.delete(container.response.id).then(function (data) {
                $log.info("response with id " + container.response.id + " success deleted")
            });
            var index = $scope.holder.indexOf(container);
            $scope.holder.splice(index, 1);
            $scope.reset();
        }

    }
});