'use strict';

keyword.component('responseItem', {
    templateUrl: "modules/talkkb/response/list/response-item.template.html",
    bindings: {
        container: '=',
        holder: '=',
        level: '<'
    },
    require: {
        responseList: '^^responseList'
    },
    controller: function ($scope, $routeParams, responseService, $log, messageService) {
        var $ctrl = this;

        $scope.container = {};
        $scope.holder = [];
        $scope.level = angular.isUndefined(this.level) ? 0 : this.level;

        $scope.selectResponse = function (container, holder) {
            $ctrl.responseList.setSelectedResponse(container, holder);
        };

        /*$scope.addRoot = function () {
            $ctrl.responseList.setIsAddRoot(true);
        };*/

        /*this.$onInit = function () {
         $scope.keywordView = this.keywordView;
         };*/

        $scope.$watch('$ctrl.container', function(newValue) {
            if (angular.isObject(newValue)) {
                $scope.container = newValue;
            }
        });
        $scope.$watch('$ctrl.holder', function(newValue) {
            if (angular.isObject(newValue)) {
                $scope.holder = newValue;
            }
        });
    }
});