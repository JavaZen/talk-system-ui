'use strict';

var response = angular.module('response', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination', 'oi.select']);

response.factory('responseService', function ($http, $log, backendUrl) {
    return {
        addResponse : function(response) {
            return $http.post(backendUrl + '/response/add', response)
                .then(function (response) {
                    return response.data;
                });

        },
        getAllByKbId: function (kbId) {
            return $http.get(backendUrl + '/response/get/all/kb' + kbId)
                .then(function(response) {
                    return response.data;
                });
        },
        getTreeByKbId: function (kbId) {
            return $http.get(backendUrl + '/response/get/tree/kb' + kbId)
                .then(function(response) {
                    return response.data;
                });
        },
        getResponseTypes: function () {
            return $http.get(backendUrl + '/response/types/get/all')
                .then(function(response) {
                    return response.data;
                });
        },
        update : function(response) {
            return $http.post(backendUrl + '/response/update', response)
                .then(function (response) {
                    return response.data;
                });

        },
        delete : function(responseId) {
            return $http.post(backendUrl + '/response/' + responseId + '/delete', {})
                .then(function (response) {
                    return response.data;
                });

        },
        getFlags: function () {
            return $http.get(backendUrl + '/response/flags/get/all')
                .then(function(response) {
                    return response.data;
                });
        },
        typesOfRelations: function () {
            return $http.get(backendUrl + '/response/typesOfRelations/get/all')
                .then(function(response) {
                    return response.data;
                });
        }
    };
});