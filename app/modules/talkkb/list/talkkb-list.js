'use strict';

//var groupList = angular.module('groupList', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination']);

talkkb.component('talkkbList', {
    templateUrl: "modules/talkkb/list/talkkb-list.template.html",
    controller: function ($scope, $http, $log, talkkbService) {
        $scope.talkkbs;

        talkkbService.getAll().then(function(data) {
            $scope.talkkbs = data;
        });

        $scope.sortColumn = 'revenues_year';
        $scope.reverseSort = false;

        $scope.sortData = function(column) {
            this.reverseSort = (this.sortColumn == column) ? !this.reverseSort : this.reverseSort;
            this.sortColumn = column;
        };

        $scope.getSortClass = function (column) {
            if (this.sortColumn == column) {
                return this.reverseSort ? 'fa fa-sort-desc' : 'fa fa-sort-asc';
            }
            return 'fa fa-sort ';
        };

        $scope.nameFilterPopover = {
            templateUrl: 'modules/task/list/popover/title-search.popover.template.html',
            title: 'Filter by Name'
        };

        $scope.revenuesYearFilterPopover = {
            templateUrl: 'modules/task/list/popover/student-search.popover.template.html',
            title: 'Filter by Student'
        };

        $scope.progressFilterPopover = {
            templateUrl: 'modules/task/list/popover/grade-search.popover.template.html',
            title: 'Filter by Grade'
        };

        $scope.statusFilterPopover = {
            templateUrl: 'modules/task/list/popover/status-search.popover.template.html',
            title: 'Filter by Status'
        };
    }
});

group.filter("groupStatus", function () {
    return function (status) {
        switch (status) {
            case 0:
                return "В процессе";
            case 1:
                return "Выполнено";
            default:
                return "N/A";
        }
    }
});