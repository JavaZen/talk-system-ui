'use strict';

var talkkb = angular.module('talkkb', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination', 'zen.message']);

talkkb.component('talkkb', {
    templateUrl: "modules/talkkb/talkkb.template.html",
    controller: function ($scope, $routeParams, talkkbService, messageService) {
        $scope.kbase;

        $scope.id = $routeParams.talkkbId;

        $scope.downloadKb = function () {
            talkkbService.download($scope.id);
        };

        talkkbService.getById($scope.id).then(function(data) {
            $scope.kbase = data;
        });
    }
});


talkkb.factory('talkkbService', function ($http, $log, backendUrl) {
    return {
        getById: function (id) {
            return $http.get(backendUrl + '/knowledge_base/' + id + '/get')
                .then(function(response) {
                    return response.data;
                });
        },
        getAll: function () {
            return $http.get(backendUrl + '/knowledge_base/get/all')
                .then(function(response) {
                    return response.data;
                });
        },
        add: function (kbase) {
            return $http.post(backendUrl + '/knowledge_base/add', kbase)
                .then(function(response) {
                    return response.data;
                });
        },
        delete: function (id) {
            return $http.post(backendUrl + '/knowledge_base/' + id + '/delete', {})
                .then(function(response) {
                    return response.data;
                });
        },
        download: function (id) {
            return $http.get(backendUrl + '/knowledge_base/' + id + '/download')
                .then(function(response) {

                    var filename = 'KnowledgeBase_' + id;
                    var contentType = 'text/xml';



                    var blob = new Blob([response.data], { type: contentType });

                    if (window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, filename + '.xml');
                    } else {

                        var linkElement = document.createElement('a');
                        var url = window.URL.createObjectURL(blob);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", filename);

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }

                });
        }
    };
});

user.config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/talkkbs', {
            template: '<talkkb-list></talkkb-list>'
        }).
        when('/talkkb/add', {
            template: '<talkkb-add></talkkb-add>'
        }).
        when('/talkkb/:talkkbId', {
            template: '<talkkb></talkkb>'
        });
    }
]);


talkkb.directive('username', function($q, $timeout, userService) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {

            ctrl.$asyncValidators.username = function(modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.resolve();
                }

                var def = $q.defer();

                userService.checkUsername(modelValue).then(function (free) {
                    if (free) {
                        def.resolve();
                    } else {
                        def.reject();
                    }
                });

                return def.promise;
            };
        }
    };
});