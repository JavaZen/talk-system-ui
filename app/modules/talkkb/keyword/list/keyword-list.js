'use strict';

keyword.component('keywordList', {
    templateUrl: "modules/talkkb/keyword/list/keyword-list.template.html",
    bindings: {
        kbaseId: '@'
    },
    controller: function ($scope, $routeParams, keywordService, $log, messageService, talkkbService, userService) {
        var $ctrl = this;
        $ctrl.kb = {};

        /*var Keyword = function () { };
        Keyword.prototype = { id: undefined, text: undefined, part_of_speech: undefined, knowledge_base_id: undefined, related_keywords: [] };
*/
        $log.info('ID = ' + this.kbaseId);
        $scope.kbaseId = this.kbaseId;
        $scope.iframeHeight = window.innerHeight;
        $scope.keywords = [];
        $scope.flags = [];

        $scope.currentUser = userService.getCurrentUser();

        $scope.isAdmin = userService.hasRole("ROLE_ADMIN");

        talkkbService.getById($scope.kbaseId).then(function (data) {
            $ctrl.kb = data;
            $scope.kb = data;
        });

        $ctrl.templateContainer = {
            "relation_id": undefined,
            "keyword": {
                "id": undefined,
                "text": undefined,
                "flags": [],
                "knowledge_base_id": undefined,
                "related_keywords": []
            },
            "type_of_relation": undefined,
            "root": undefined
        };

        $ctrl.editCtrlStates = [{value:'none', show:false}, {value:'edit', show:true},
            {value:'add_child', show:true}, {value:'add_root', show:true}];

        $scope.containter = undefined;
        $scope.editContainer = undefined;
        $scope.holder = [];
        $scope.editView = $ctrl.editCtrlStates[0];

        $scope.typesOfRelation = [];

        keywordService.getTypesOfRelation().then(function (data) {
            $scope.typesOfRelation = data;
        });

        keywordService.getFlags().then(function (data) {
            $scope.flags = data;
        });

        $scope.reset = function () {
            $scope.editView = $ctrl.editCtrlStates[0];
            $scope.container = undefined;
            $scope.editKeyword = undefined;
            $scope.editKeywordForm.$setPristine();
            $scope.editKeywordForm.$setUntouched();
        };

        $scope.edit = function (isEdit) {
            $scope.editView = $ctrl.editCtrlStates[isEdit?1:0];
            $scope.editContainer = isEdit ? $scope.container : undefined;
        };

        $scope.addChild = function (isAdd) {
            $scope.editView = $ctrl.editCtrlStates[isAdd?2:0];
            $scope.editContainer = angular.copy($ctrl.templateContainer);
        };

        $scope.addRoot = function (isAdd) {
            if (isAdd) {
                $scope.editContainer = angular.copy($ctrl.templateContainer);
                $scope.container = angular.copy($ctrl.templateContainer);
                $scope.editView = $ctrl.editCtrlStates[3];
            } else {
                $scope.reset();
            }
        };

        keywordService.getTreeByKbId($scope.kbaseId).then(function(data) {
            $scope.keywords = data;
        });

        $ctrl.setSelectedWord = function (container, containers) {
            $scope.reset();
            $scope.container = container;
            $scope.holder = containers;
        };

        $ctrl.setIsAddRoot = function (isAdd) {
            $scope.addRoot(isAdd);
        };

        $scope.addRootKeyword = function () {
            if (!$scope.editKeywordForm.$valid) {
                return;
            }
            var cont = $scope.editContainer;
            cont.keyword.knowledge_base_id = $scope.kbaseId;

            keywordService.addKeyword(cont.keyword)
                .then(function (data) {
                    var newKeywordId = data;
                    cont.keyword.id = newKeywordId;

                    $scope.keywords.push(cont);
                });

            $scope.reset();
        };

        $scope.saveChanges = function () {
            if (!$scope.editKeywordForm.$valid) {
                return;
            }

            keywordService.update($scope.editContainer.keyword);

            var relation = {
                relation_id: $scope.editContainer.relation_id,
                type_of_relation: $scope.editContainer.type_of_relation };

            keywordService.updateRelation(relation);

            $scope.reset();
        };

        $scope.remove = function () {
            $('#deletePopup').modal('hide');
            var idForRemove = $scope.container.keyword.id;

            //var index = $scope.collectionForSelectedWord.findIndex(e => e.keyword == $scope.selectedWord); // only for ecma2015
            var index = -1;
            for (var i = 0; i< $scope.holder.length; i++) {
                if ($scope.holder[i].keyword == $scope.container.keyword) {
                    index = i;
                    break;
                }
            }
            var w = $scope.holder.splice(index, 1);
            $log.info("keywordList#remove w ", w);
            $scope.reset();


            $log.info("remove: " + idForRemove);
            keywordService.delete(idForRemove);
        };

        $scope.addWord = function () {
            if (!$scope.editKeywordForm.$valid) {
                return;
            }
            $log.info('container', $scope.editContainer);
            $log.info('selectedWord', $scope.container);

            var keyword = $scope.editContainer.keyword;
            $log.info('keyword', keyword);

            keywordService.addKeyword(keyword)
                .then(function (data) {
                    var newKeywordId = data;
                    $log.info("newKeywordId", newKeywordId);

                    var relation = {
                        keyword_from_id: $scope.container.keyword.id,
                        keyword_to_id: newKeywordId,
                        type_of_relation: $scope.editContainer.type_of_relation
                    };
                    
                    $log.info('relation', relation);
                    keywordService.saveRelation(relation)
                        .then(function (data) {
                            $log.info('new relation', data);
                            var relationId = data;

                            $scope.editContainer.relation_id = relationId;

                            $scope.container.keyword.related_keywords.push(
                                /*{ keyword: { id: newKeywordId, text: container.word.text, part_of_speech: container.word.part_of_speech, related_keywords: [] },
                                    relation_id: relationId, type_of_relation: container.type_of_relation }*/
                                $scope.editContainer
                            );
                            $scope.editView = $ctrl.editCtrlStates[0];
                            $scope.editContainer = undefined;

                        });
                });
        }

    }
});