'use strict';

keyword.component('keywordItem', {
    templateUrl: "modules/talkkb/keyword/list/keyword-item.template.html",
    bindings: {
        containers: '=',
        level: '<'
    },
    require: {
        keywordList: '^^keywordList'
    },
    controller: function ($scope, $routeParams, keywordService, $log, userService) {
        var $ctrl = this;

        $scope.keywordContainers = [];
        $scope.level = angular.isUndefined(this.level) ? 0 : this.level;

        $scope.currentUser = userService.getCurrentUser();
        $scope.isAdmin = userService.hasRole("ROLE_ADMIN");

        $scope.selectWord = function (container, containers) {
            $ctrl.keywordList.setSelectedWord(container, containers);
        };

        $scope.addRoot = function () {
            $ctrl.keywordList.setIsAddRoot(true);
        };

        //$scope.kb = $ctrl.keywordList.kb;

        /*this.$onInit = function () {
            $scope.keywordView = this.keywordView;
        };*/

        $scope.$watch('$ctrl.containers', function(newValue) {
            if (angular.isObject(newValue)) {
                $log.info("keyword#keywordItem: keyword container", newValue);
                $scope.keywordContainers = newValue;
            }
        });
    }
});