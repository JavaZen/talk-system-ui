'use strict';

var keyword = angular.module('keyword', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination', 'oi.select']);

keyword.factory('keywordService', function ($http, $log, backendUrl) {
    return {
        addKeyword : function(keyword) {
            return $http.post(backendUrl + '/keyword/add', keyword)
                .then(function (response) {
                    $log.info(response);
                    return response.data;
                });

        },
        saveRelation: function(relation) {
            return $http.post(backendUrl + '/keyword/relate/k2k/save', relation)
                .then(function (response) {
                    return response.data;
                });
        },
        updateRelation: function (relation) {
            return $http.post(backendUrl + '/keyword/relate/k2k/update', relation)
                .then(function (response) {
                    return response.data;
                })
        },
        saveK2RRelation: function(relation) {
            return $http.post(backendUrl + '/keyword/relate/k2r/save', relation)
                .then(function (response) {
                    return response.data;
                });
        },
        unrelateK2RRelation: function(keywordId, responseId) {
            return $http.post(backendUrl + '/keyword/unrelate/keyword'+ keywordId + '/response' + responseId, 'stub')
                .then(function (response) {
                    return response.data;
                });
        },
        getById: function (id) {
            return $http.get(backendUrl + '/keyword/' + id + '/get')
                .then(function(response) {
                    return response.data;
                });
        },
        getTreeById: function (id) {
            return $http.get(backendUrl + '/keyword/' + id + '/get/tree')
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        getListByResponseId: function (responseId) {
            return $http.get(backendUrl + '/keyword/get/list/response' + responseId)
                .then(function (response) {
                    $log.info(response);
                    return response.data;
                })
        },
        getTreeByResponseId: function (responseId) {
            return $http.get(backendUrl + '/keyword/get/tree/response' + responseId)
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        getListByKbId: function (kbId) {
            return $http.get(backendUrl + '/keyword/get/list/kb' + kbId)
                .then(function (response) {
                    $log.info(response);
                    return response.data;
                })
        },
        getTreeByKbId: function (kbId) {
            return $http.get(backendUrl + '/keyword/get/tree/kb' + kbId)
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        delete: function (keywordId) {
            return $http.post(backendUrl + '/keyword/' + keywordId + '/delete', keywordId)
                .then(function (response) {
                    return response.data;
                });
        },
        update: function (keyword) {
            return $http.post(backendUrl + '/keyword/update', keyword)
                .then(function (response) {
                    return response.data;
                });
        },
        getTypesOfRelation: function () {
            return $http.get(backendUrl + '/keyword/typeOfRelation/get/all')
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        },
        getFlags: function () {
            return $http.get(backendUrl + '/keyword/flags/get/all')
                .then(function(response) {
                    $log.info(response);
                    return response.data;
                });
        }
    };
});