'use strict';

var conversation = angular.module('conversation', ['ui.bootstrap', 'ui.bootstrap.popover', 'angularUtils.directives.dirPagination']);

conversation.component('conversation', {
    templateUrl: "modules/talkkb/conversation/conversation.template.html",
    bindings: {
        chatId: '='
    },
    controller: function ($scope, $routeParams, $log, longPolling, messageService, conversationService, userService) {
        var $ctrl = this;

        $scope.chatId = this.chatId;
        
        $scope.userMessage = "";
        $scope.messages = [];
        $scope.chat = {};
        $scope.currentUserId = userService.getCurrentUser().id;
        $scope.participants = [];

        $scope.showExplanation = false;
        $scope.explanations = [];
        $scope.detail = {};

        $scope.resetContext = function () {
            conversationService.resetContext($scope.chatId)
        };

        $scope.explain = function (isShow) {
            $scope.showExplanation = isShow;
        };

        $scope.showTrace = function (detail) {
            $scope.detail = detail;
        };

        $scope.chatContainer = document.getElementById('chat-body-panel');

        $scope.$watch('chatContainer.scrollHeight', function () {
            setTimeout(function () {
                //todo... but I don't know, how better scroll to bottom after receiving all messages
                $scope.chatContainer.scrollTop = $scope.chatContainer.scrollHeight;
            }, 50); //todo why delay is occurs after printing all messages
        });

        $scope.$watch('$ctrl.chatId', function(newValue) {
            if (angular.isNumber(newValue) && newValue != 0) {
                $scope.chatId = newValue;

                conversationService.getChatById($scope.chatId)
                    .then(function (data) {
                        $scope.chat = data;
                        var chatId = data.id;

                        $scope.participants = [];
                        $scope.chat.participants.forEach(function (participant) {
                            $scope.participants[participant.user_id] = participant;
                        });

                        conversationService.getLastUpdates(chatId, 20)
                            .then(function (updates) {
                                $scope.messages = updates;
                            })
                    });

                var onExplain = function (update) {
                    if (update.chatId == $scope.chatId) {
                        $scope.explanations.push(update);
                    }
                };

                longPolling.onUpdate(onExplain, "explanation_chat_" + $scope.chatId);
            }
         });

        $scope.sendMessage = function () {
            if (!$scope.userMessage) {
                return;
            }

            var update = { chat_id: $scope.chatId, message: $scope.userMessage, random_id: 111 };
            conversationService.sendUpdate(update);
            $scope.userMessage = '';
        };

        var onMessage = function (chatUpdate) {
            if (chatUpdate.chat_id == $scope.chatId) {
                $scope.messages.push(chatUpdate);
            }
        };

        longPolling.onUpdate(onMessage, "chat");

    }
});

conversation.factory('conversationService', function ($http, $log, backendUrl) {
    return {
        createConversation : function(chat) {
            return $http.post(backendUrl + '/chat/create', chat)
                .then(function (response) {
                    return response.data;
                });

        },
        sendUpdate : function(response) {
            return $http.post(backendUrl + '/chat/update/send', response)
                .then(function (response) {
                    return response.data;
                });

        },
        resetContext: function(id) {
            return $http.post(backendUrl + '/chat/' + id +'/reset', {})
                .then(function (response) {
                    return response.data;
                });

        },
        getLastUpdates: function (chatId, updatesCount) {
            return $http.get(backendUrl + '/chat/' + chatId + '/update/get/last' + updatesCount)
                .then(function(response) {
                    return response.data;
                });
        },
        getUpdatesWithOffset: function (chatId, updatesCount, offsetMessageId) {
            return $http.get(backendUrl + '/chat/' + chatId + '/update/get/count' + updatesCount + '/offset' + offsetMessageId)
                .then(function(response) {
                    return response.data;
                });
        },
        getUpdatesFromUpdate: function (chatId, fromUpdateId) {
            return $http.get(backendUrl + '/chat/' + chatId + '/update/last/from' + fromUpdateId)
                .then(function(response) {
                    return response.data;
                });
        },
        getChatByUsers: function (firstUserId, secondUserId) {
            return $http.get(backendUrl + '/chat/get/users/first' + firstUserId + '/second' + secondUserId)
                .then(function(response) {
                    return response.data;
                });
        },
        getChatById: function (id) {
            return $http.get(backendUrl + '/chat/' + id + '/get')
                .then(function(response) {
                    return response.data;
                });
        }
    };
});