'use strict';

conversation.component('conversationList', {
    templateUrl: "modules/talkkb/conversation/list/conversation-list.template.html",
    bindings: {
        userId: '='
    },
    controller: function ($scope, $http, $log, conversationService, userService) {
        $scope.userId = this.userId;

        $scope.iframeHeight = window.innerHeight;

        $scope.chats = [];
        $scope.selectedChatId = 0;

        $scope.newChat = {};

        conversationService.getChatByUsers(userService.getCurrentUser().id, $scope.userId)
            .then(function (data) {
                $scope.chats = data;
            });

        $scope.selectChat = function (chatId) {
            $scope.selectedChatId = chatId;
        };
        
        $scope.createChat = function (newChat) {
            newChat.members = [$scope.userId];
            conversationService.createConversation(newChat).then(function (chat) {
                $scope.chats.push(chat);
                $scope.newChat.title = "";
            });
            $('#createChat').modal('hide');
        };

    }
});