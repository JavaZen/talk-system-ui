'use strict';

    var longPolling = angular.module('longPolling', []);

longPolling.factory('longPolling', function ($http, $log, messageService, backendUrl) {
    var onUpdateHandlers = [];
    var attempts = 0;
    var lastId = 0;
    var main = { poll: function () { }};

    main.poll = function(){
        $http.get(backendUrl + '/longpolling' + (lastId ==  0 ? '' : '?from_id=' + lastId))
            .success(function(updates) {
                $log.info("updates", updates);
                if (updates != []) {
                    updates.forEach(function (update) {
                        onUpdateHandlers.forEach(function (handler) {
                            if (lastId < update.id) { lastId = update.id; }
                            if (update.type == handler.type) {
                                handler.call(update.data);
                            }
                        });
                    });
                }
                main.poll();
            })
            .error(function (error) {
                setTimeout(main.poll, 3000);
            })
    };

    main.poll();

    return {
        onUpdate: function (handler, type) {
            onUpdateHandlers.push({ type: type, call: handler});
        }
    }
});