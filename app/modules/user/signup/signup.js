'use strict';

var signUp = angular.module('signUp', ['ui.bootstrap']);

signUp.component('signUp', {
    templateUrl: "modules/user/signup/signup.template.html",
    controller: function ($scope, $http, $log, userService) {

        $scope.username;
        $scope.password;
        $scope.confirmPassword;
        $scope.full_name;
        $scope.description;

        $scope.signup = function () {
            if (!$scope.userForm.$valid) {
                return;
            }
            if ($scope.password == $scope.confirmPassword) {
                userService.signup({
                    username: $scope.username,
                    password: $scope.password,
                    full_name: $scope.full_name,
                    description: $scope.description
                });
            }
        };
    }
});