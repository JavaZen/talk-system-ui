var user = angular.module('user', ['oi.select']);

user.component('profile', {
    templateUrl: "modules/user/user.template.html",
    controller: function ($scope, $routeParams, $http, $log, userService, messageService) {

        $scope.id = $routeParams.userId;

        userService.getUserById($scope.id).then(function (user) {
            $scope.user = user;
        });

        $scope.userBeforeEdit = {};

        $scope.roles = [];

        $scope.groups = [];

        $scope.password = "";
        $scope.confirmPassword = "";
        $scope.oldPassword = "";

        $scope.isAdmin = userService.hasRole("ROLE_ADMIN");
        $scope.currentUser = userService.getCurrentUser();

        userService.getAllRoles().then(function (response) {
            $scope.roles = response;
        });

        userService.getAllGroups().then(function (response) {
            $scope.groups = response;
        });


        $scope.isEditProfile = false;

        $scope.edit = function () {
            $scope.userBeforeEdit = angular.copy($scope.user);

            $scope.isEditProfile = true;
        };

        $scope.save = function () {
            if (!$scope.userForm.$valid) {
                return;
            }

            if ($scope.password != $scope.confirmPassword
                && $scope.password != "" && $scope.confirmPassword != "") {
                messageService.alert.warning("Пароли не совпадают");
                return;
            }
            var roleIds = [];
            $scope.user.roles.forEach(function (role) {
                roleIds.push(role.id);
            });

            var user = { 
                id: $scope.user.id,
                username: $scope.user.username,
                full_name: $scope.user.full_name,
                description: $scope.user.description,
                group_id: $scope.user.group.id,
                role_ids: roleIds,
                old_password: $scope.oldPassword
            };

            if ($scope.password == $scope.confirmPassword && $scope.password != "") {
                user.password = $scope.password;
            }

            userService.updateUser(user);
            $scope.isEditProfile = false;
        };

        $scope.cancel = function () {
            $scope.userForm.$setPristine();
            $scope.userForm.$setUntouched();

            $scope.user = angular.copy($scope.userBeforeEdit);
            $scope.isEditProfile = false;
        };
    }
});

user.directive('username', function($q, $timeout, userService) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {

            ctrl.$asyncValidators.username = function(modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.resolve();
                }

                var def = $q.defer();

                userService.checkUsername(modelValue).then(function (free) {
                    if (free) {
                        def.resolve();
                    } else {
                        def.reject();
                    }
                });

                return def.promise;
            };
        }
    };
});

user.directive('password', function($q, $timeout, userService) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {

            ctrl.$asyncValidators.password = function(modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.resolve();
                }

                var def = $q.defer();

                userService.checkCurrentPassword(modelValue).then(function (correct) {
                    if (correct) {
                        def.resolve();
                    } else {
                        def.reject();
                    }
                });

                return def.promise;
            };
        }
    };
});

user.directive('match', function() {
    return {
        require: 'ngModel',
        scope: {
            otherModelValue: "=match"
        },
        link: function(scope, elm, attrs, ctrl, ngModel) {

            ctrl.$validators.match = function(modelValue, viewValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ctrl.$validate();
            });
        }
    };
});

user.directive('allowed', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl, ngModel) {

            ctrl.$validators.allowed = function(modelValue, viewValue) {
                return /^[a-zA-Z0-9_]+$/.test(modelValue);
            };
        }
    };
});

user.factory('userService', function ($http, $log, $location, messageService, backendUrl) {

    var ctrl = {
        getCurrentUser: function () {
            var userData = sessionStorage.getItem('user_data');
            return JSON.parse(userData);
        },
        setCurrentUser: function (user) {
            sessionStorage.setItem('user_data', JSON.stringify(user));
        },
        isAuthenticated: function () {
            return angular.isString(sessionStorage.getItem('user_data'));
        },
        hasRole: function (role) {
            var isHasRole = false;
            ctrl.getCurrentUser().roles.forEach(function (userRole){
                if (userRole.authority.toUpperCase() === role.toUpperCase()) { //todo - case sensitive? and... ROLE_ADMIN == ADMIN - todo....
                    isHasRole = true;
                }
            });
            return isHasRole;
        },
        userHasRole: function (user, role) {
            var isHasRole = false;
            user.roles.forEach(function (userRole){
                if (userRole.authority.toUpperCase() === role.toUpperCase()) { //todo - case sensitive? and... ROLE_ADMIN == ADMIN - todo....
                    isHasRole = true;
                }
            });
            return isHasRole;
        },
        signin: function(name, password, callback) {

            sessionStorage.setItem('Authorization', "Basic " + btoa(name + ":" + password));

            $http.get(backendUrl + '/auth/user').then(function(response) {
                var user = response.data;
                ctrl.setCurrentUser(user);
                callback(true);
            }, function() {
                callback(false);
            });
        },
        signup: function (user) {
            $http.post(backendUrl + "/auth/signup", user)
                .then(function (response) {
                    $location.path('/signin');
                }, function() {
                    messageService.alert.danger("Registration failed");
                });
        },
        logout: function () {
            sessionStorage.clear();
            $location.path("/signin");
        },
        getAllRoles: function() {
            return $http.get(backendUrl + '/user/get/role/all').then(function (response) {
                return response.data;
            });
        },
        getAllGroups: function() {
            return $http.get(backendUrl + '/user/get/group/all').then(function (response) {
                return response.data;
            });
        },
        getUserById: function(id) {
            return $http.get(backendUrl + '/user/' + id + '/get').then(function (response) {
                return response.data;
            });
        },
        getUsersByGroupId: function(id) {
            return $http.get(backendUrl + '/user/get/group' + id).then(function (response) {
                return response.data;
            });
        },
        getAllUsers: function() {
            return $http.get(backendUrl + '/user/get/all').then(function (response) {
                return response.data;
            });
        },
        checkUsername: function(username) {
            return $http.get(backendUrl + '/user/username/free?username=' + username).then(function (response) {
                return response.data;
            });
        },
        updateUser: function(user) {
            return $http.post(backendUrl + '/user/update', user).then(
                function (response) {
                    return response.data;
                },
                function (error) {
                    messageService.alert.danger("Внутренняя ошибка сервера. Для получения деталей обратитесь к администратору");
                    return error;
                }
            );
        },
        checkCurrentPassword: function(password) {
            return $http.post(backendUrl + '/user/password/check', password).then(function (response) {
                return response.data;
            });
        }
    };

    return ctrl;
});

user.config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/users', {
            template: '<user-list></user-list>'
        });
    }
]);