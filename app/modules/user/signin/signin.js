'use strict';

var signIn = angular.module('signIn', ['ui.bootstrap']);

signIn.component('signIn', {
    templateUrl: "modules/user/signin/signin.template.html",
    controller: function ($scope, $http, $log, $location, messageService, userService) {

        $scope.username;
        $scope.password;

        $scope.incorrect = false;

        $scope.signin = function () {
            if (!$scope.userForm.$valid) {
                return;
            }
           /* messageService.alert.info("login " + $scope.username + " " + $scope.password);*/
            userService.signin($scope.username, $scope.password, function (success) {
                if (success) {
                    // todo redirect
                    $location.path('/profile/' + userService.getCurrentUser().id)
                } else {
                    $scope.incorrect = true;
                }
            });
        }
    }
});