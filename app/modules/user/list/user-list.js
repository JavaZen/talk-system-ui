'use strict';

user.component('userList', {
    templateUrl: "modules/user/list/user-list.template.html",
    bindings: {
        groupId: '@'
    },
    controller: function ($scope, $http, $log, userService) {
        $scope.groupId = this.groupId;
        $scope.users;
        $scope.isShowBot = $scope.groupId == 3; //TODO

        $scope.isBot = function(user) {
            return userService.userHasRole(user, 'ROLE_BOT');
        };
        
        if (angular.isDefined($scope.groupId)) {
            userService.getUsersByGroupId($scope.groupId).then(function (data) {
                $scope.users = data;
            });
        } else {
            userService.getAllUsers().then(function (data) {
                $scope.users = data;
            });
        }

        $scope.sortColumn = 'username';
        $scope.reverseSort = false;

        $scope.sortData = function(column) {
            this.reverseSort = (this.sortColumn == column) ? !this.reverseSort : this.reverseSort;
            this.sortColumn = column;
        };

        $scope.getSortClass = function (column) {
            if (this.sortColumn == column) {
                return this.reverseSort ? 'fa fa-sort-desc' : 'fa fa-sort-asc';
            }
            return 'fa fa-sort ';
        };
    }
});