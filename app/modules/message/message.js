'use strict';

/**
 * Message module for different messages and notifications on UI path of the system
 * @type {angular.Module}
 */
var zenMessage = angular.module('zen.message', ['ui.bootstrap']);

zenMessage.factory('messageService', function ($http, $log) {
    var onAlertHandlers = [];
    var messageIds = 0;

    return {
        onAlert : function(handler) {
            onAlertHandlers.push({ call: handler });
        },
        alert: {
            warning: function (text) {
                this.show(text, 'warning');
            },
            success: function (text) {
                this.show(text, 'success')
            },
            info: function (text) {
                this.show(text, 'info');
            },
            danger: function (text) {
                this.show(text, 'danger');
            },
            show: function (text, type) {
                var message = { id: messageIds++, type: type, text: text};

                onAlertHandlers.forEach(function (handler) {
                    handler.call(message);
                });
            }

        }
    };
});