'use strict';

zenMessage.component('alertMessage', {
    templateUrl: "modules/message/alert/alert-message.template.html",
    controller: function ($scope, $log, messageService) {
        
        $scope.messages = [];

        $scope.dismissMessage = function (message) {
            var index = $scope.messages.indexOf(message);
            $scope.messages.splice(index, 1);
        };

        this.messageSubscriber = function (message) {
            $scope.messages.push(message);
        };

        messageService.onAlert(this.messageSubscriber);

    }
});